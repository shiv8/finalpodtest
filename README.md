# finalPodTest

[![CI Status](https://img.shields.io/travis/Shiv Prakash/finalPodTest.svg?style=flat)](https://travis-ci.org/Shiv Prakash/finalPodTest)
[![Version](https://img.shields.io/cocoapods/v/finalPodTest.svg?style=flat)](https://cocoapods.org/pods/finalPodTest)
[![License](https://img.shields.io/cocoapods/l/finalPodTest.svg?style=flat)](https://cocoapods.org/pods/finalPodTest)
[![Platform](https://img.shields.io/cocoapods/p/finalPodTest.svg?style=flat)](https://cocoapods.org/pods/finalPodTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

finalPodTest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'finalPodTest'
```

## Author

Shiv Prakash, shiv@kruzr.co

## License

finalPodTest is available under the MIT license. See the LICENSE file for more info.
